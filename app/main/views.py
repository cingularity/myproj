from datetime import datetime
from flask import render_template, session, redirect, url_for
from flask_login import login_required


from . import main
from .. import db
from ..models import User
from .forms import NameForm



@main.route('/', methods=['GET', 'POST'])
def index():
    form = NameForm() 
    if form.validate_on_submit():
        session['name'] = form.name.data
        return redirect(url_for('.index'))
    return render_template('index.html', form=form, name=session.get('name'))


@main.route('/secret')
@login_required
def secret():
    return 'Only authenticated users are allowed!'
