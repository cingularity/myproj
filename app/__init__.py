from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

# this is the import from our config file, take care,
# we are import the dictionary from the config.py
from config import config 


# instantiating all the requirements

bootstrap = Bootstrap() 
mail = Mail()
moment = Moment()
db = SQLAlchemy()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    
    
    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    
    
    # attach routes and custom error pages here
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    
    from .proj import proj as proj_blueprint
    app.register_blueprint(proj_blueprint, url_prefix='/proj')
    
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefic='/auth')
    
    return app