from flask import render_template
from . import proj


@proj.route("/")
def index():
    return render_template('proj/index.html')